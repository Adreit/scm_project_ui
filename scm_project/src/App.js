import React, { Component } from 'react';
import {Route} from 'react-router-dom'
import Header from './Component/fragments/header'
import Content from './Component/fragments/content'
import Footer from './Component/fragments/footer'
import HomePage from './Component/pages/homepage'
import DetailedTable from './Component/pages/fc_table'
import './index.css'

class App extends Component {
  render() {
    return (
      <div >
        <Header/>
        <Content>
           <Route path="/" exact component={HomePage}/>
           <Route path="/fc_details" exact component={DetailedTable}/>
        </Content>
        <Footer/>
      </div>
    );
  }
}

export default App;
