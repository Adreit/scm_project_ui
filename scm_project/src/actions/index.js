
const BASE_URL = "http://127.0.0.1:5000/fc_details"

function get_information(selectedOption,date){
  let url = `${BASE_URL}/?fc=${selectedOption}&date=${date}`;

  return fetch(url, {
    method: "GET",
   headers:{
      Accept: 'application/json',
     'Content-Type': 'application/json'}
  }).then(res => res.json())
  .catch(err => {
    throw new Error(err)
  })
}
export {get_information};