import React from 'react';
import { get_information } from '../../actions/index'
import {Button } from 'semantic-ui-react';
import 'isomorphic-fetch';
import ReactTable from 'react-table';



class DetailedTable extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading:false,
      selectedOption:'',
      date:''
    };
    this.renderEditable = this.renderEditable.bind(this);
    this.submit_cell = this.submit_cell.bind(this)
  }
  submit_cell(cellInfo) {
    const data = this.state.data;
    return (
      <Button
        onClick={e => {
          console.log(`this cell is to be submitted+${cellInfo.index}:`,data[cellInfo.index])
          fetch('http://127.0.0.1:5000/update', {
            method: 'POST',
            headers: {'Content-Type':'application/json',
                     'Accept': 'application/json'
                      }, // this line is important, if this content-type is not set it wont work
            body: JSON.stringify(data[cellInfo.index])
        })
        .then(res => res.json()).
        then(res => console.log(res));
        }}
      > Update </Button>
    );
  }

  renderEditable(cellInfo) {
    return (
      <div
        style={{ backgroundColor: "#fafafa" }}
        contentEditable
        suppressContentEditableWarning
        onBlur={e => {
          const data = [...this.state.data];
          data[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
          console.log(`this cell  was updated+${cellInfo.index}:`,data[cellInfo.index])
          this.setState({ data });
        }}
        dangerouslySetInnerHTML={{
          __html: this.state.data[cellInfo.index][cellInfo.column.id]
        }}
      />
    );
  }


  componentDidMount() {
    const selectedOption = this.props.location.state.selectedOption.value
    const date = this.props.location.state.date.toLocaleDateString()
    console.log(`my values`,date,selectedOption)
    get_information(selectedOption, date)
    .then(res => this.setState({loading: false, data: res}));
  }
  render(){
    const { data } = this.state;
    return(
      <div>
        <ReactTable
        data={data}
        filterable
        defaultFilterMethod={(filter, row) =>
          String(row[filter.id]) === filter.value}
        columns={[
          {
            Header: "PO",
            accessor: "PO",
          },
          {
            Header: "SHIPMENT_NBR",
            accessor: "SHIPMENT_NBR",
            Cell: this.renderEditable
          },
          {
            Header: "BOL",
            accessor: "BOL",
          },
          {
            Header: "Staples_BOL",
            accessor: "Staples_BOL",
            Cell: this.renderEditable
          },
          {
            Header: "Source_FC",
            accessor: "Source_FC",
          },
          {
            Header: "Destination_FC",
            accessor: "Destination_FC",
            Cell: this.renderEditable
          },
          {
            Header: "Pallets",
            accessor: "Pallets",
          },
          {
            Header: "Trailer_Number",
            accessor: "Trailer_Number",
            Cell: this.renderEditable
          },
          {
            Header: "SHIPMENT_DATE",
            accessor: "SHIPMENT_DATE",
          },
          {
            Header: "Date_Routed",
            accessor: "Date_Routed",
            Cell: this.renderEditable
          },
          {
            Header: "Date_Picked",
            accessor: "Date_Picked",
          },
          {
            Header: "PO_Type",
            accessor: "PO_Type",
            Filter: ({ filter, onChange }) =>
                    <select
                      onChange={event => onChange(event.target.value)}
                      style={{ width: "100%" }}
                      value={filter ? filter.value : "all"}
                    >
                      <option value="all">Show All</option>
                      <option value="SBG X DOCK">SBG X DOCK</option>
                      <option value="FC to FC T">FC to FC T</option>
                    </select>
          },

          {
            Header: "Update",
            accessor: "Update",
            Cell:this.submit_cell
          }
        ]}
          defaultPageSize={10}
          className="-striped -highlight"
          />
          {console.log(`update table :`,this.state.data)}
      </div>
    );
  }
}
export default DetailedTable;