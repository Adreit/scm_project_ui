import React from 'react';
import { Button } from 'semantic-ui-react';
import DatePicker from 'react-date-picker';
import Select from 'react-select';
import {fcOptions} from '../data/fc_list';

class HomePage extends React.Component{
  constructor(props){
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this)
    this.onChange = this.onChange.bind(this)
    this.navigate = this.navigate.bind(this)
    this.state = {
      selectedOption:"London",
      date: new Date()
    };
    
  }
  
  onChange = date => {
    this.setState({ date });
    console.log(`Date:`, date.toLocaleDateString());
  }
  navigate(path) {
    this.props.history.push(this.state)
    this.props.history.push(path,this.state);
  }
  onSubmit(){
    this.props.history.push(`/fc_details/:${this.state.selectedOption.value}/:${this.state.date.toLocaleDateString()}`)
    this.navigate(`/fc_details/:${this.state.selectedOption.value}/:${this.state.date.toLocaleDateString()}`)
    console.log(this.props.location.search);
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  }
  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.selectedOption);
    event.preventDefault();
    this.props.push(this.state);
  }
  handleChange = (selectedOption, date) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption.value);
  }
  
  render(){
    const { selectedOption, date } = this.state;
    
    return (
      <div className='homepage'>
        <div>
          <p>Please Select the FC</p>
          <Select style={{width:"20%"}} value={selectedOption} onChange={this.handleChange.bind(this)} options={fcOptions} />
        </div>
        <div className="date">
        <p>Enter a date</p>
          <DatePicker value={this.state.date} onChange={this.onChange.bind(this)} />
        </div>
        <br/>
        <Button style={{paddingTop:"10px"}} onClick={this.navigate.bind(this,`/fc_details`)} className="button"> View FC information
        </Button>
      </div>
        
    );
  }
}
export default HomePage;