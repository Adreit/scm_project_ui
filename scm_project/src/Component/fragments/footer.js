import React from 'react';
import { Container } from "semantic-ui-react";

class Footer extends React.Component{
  render(){
    return(
      <div className='footer'>
        <Container>
          <br/>
          <div style={{paddingLeft:'40%'}}>
            Copyright 2018
          </div>
        </Container>
      </div>
      
    );
  }
}
export default Footer;