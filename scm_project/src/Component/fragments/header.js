import React, { Component } from 'react';

import Staples_logo from './Staples_logo.png'
import { Container } from 'semantic-ui-react';

class Header extends React.Component{

  render(){
    return (
      <div className="App">
      <Container>
        <header className="App-header">
          <img src={Staples_logo} className="App-logo" alt="logo" />
          <hr style={{width:"100%"}}/>
          <h1 style={{paddingLeft:""}}className="App-title">TRACKING SBG XD</h1>
           <br/>
        </header>
      </Container>
        
      </div>
    );
  }
  
}
export default Header;